from os import environ
import time
from configparser import ConfigParser

from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from utilities.logging import get_logger
from utilities.utils import get_driver, save_to_csv

# read config file
config_parser = ConfigParser()
config_parser.read('conf/ffly_main.conf')
module_conf = config_parser["MODULE"]

# init logging
logger_name = environ.get('ffly_main_get_logger', 'ffly_main')
# for local logs storage and stdout use DEBUG_FLAG = 1
DEBUG_FLAG = environ.get('ffly_main_DEBUG_FLAG', 1)
log_level = environ.get('ffly_main_log_level', 'INFO')

logger = get_logger(logger_name, DEBUG_FLAG, log_level)

# other variables
URL = environ.get('ffly_main', module_conf.get('URL'))
browser = environ.get('ffly_main_browser', module_conf.get('browser'))

# init web driver
driver = get_driver(browser)
driver.get(URL)


def data_input(from_city, to_city, date):
    date_dict = {"01": "January", "02": "February", "03": "March", "04": "April", "05": "May", "06": "June",
                 "07": "July", "08": "August", "09": "September", "10": "October", "11": "November",
                 "12": "December"}
    date = date.split("-")
    month = date_dict[date[1]]
    month_year = month + " " + date[2]

    try:
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          ".//div[@class='fare-radio-row bordered-row "
                                                                          "three-items-row']")))
    except Exception as e:
        logger.warning(f"Exception: {e}")

    str_ways = driver.find_elements(By.XPATH, ".//div[contains(@class, 'fare-radio-row bordered-row three-items-row')]")

    for str_way in str_ways:
        str_way.find_element(By.ID, "dvRadioOneway").click()
    table_contents = driver.find_elements(By.XPATH, ".//div[contains(@class, 'tabContent')]")

    for table_content in table_contents:
        try:
            time.sleep(3)
            inputs = table_content.find_elements(By.XPATH, ".//input[contains (@class, 'blue-outline with-icon')]")
            inputs[0].send_keys(from_city)
            inputs[1].send_keys(to_city)
            try:
                table_content.find_element(By.NAME, "ctl00$c$CtWNW$txtDepartDate").click()
            except StaleElementReferenceException as e:
                logger.warning("Stale Element Reference Exception")
                continue

            while True:
                calendars = driver.find_elements(By.XPATH, ".//div[contains(@id, 'ui-datepicker-div')]")
                for calendar in calendars:
                    cal_month = calendar.find_element(By.XPATH, ".//span[@id='monthLeft']").text
                    if cal_month == month_year:
                        try:
                            calendar.find_element(By.XPATH, ".//table[contains(@class, 'ui-datepicker-calendar')]")\
                                    .find_element(By.ID, f"day-{date[0]}-{int(date[1]) - 1}-{date[2]}").click()
                        except StaleElementReferenceException as e:
                            logger.warning("Stale Element Reference Exception")
                            continue
                        return
                    else:
                        calendar.find_element(By.ID, "nextMonth").click()
                        continue

        except Exception as e:
            logger.warning(f"Error: {e}")

        time.sleep(10)


def scrape_data():
    data_list = []
    try:
        driver.find_element(By.XPATH, ".//a[@id='ctl00_c_IBE_PB_FF']").click()
    except StaleElementReferenceException as e:
        logger.warning("Stale Element Reference Exception")
        pass

    try:
        WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                          ".//div[@class='ctl00_c_divFlightList']")))
    except Exception as e:
        logger.warning(f"Exception: {e}")
    try:
        flights_rows = driver.find_elements(By.XPATH, ".//div[contains (@class, 'flights-row')]")
        flights_rows = flights_rows[:3]
    except:
        return "An error occurred: no flights found!"

    for flights_row in flights_rows:
        flight_time = flights_row.find_element(By.XPATH, ".//div[@class='ts-fie__infographic']").text.split("\n")
        flight_price = flights_row.find_element(By.XPATH, ".//div[@class='ts-ifl-row__footer-price']").text

        flight_dict = {"flight time": flight_time[0], "flight cost": flight_price}
        data_list.append(flight_dict)

    return data_list


def main():
    from_city = input("Input the start city: ")
    to_city = input("Input destination city: ")
    date = input("Input day (for example 12-01-2022): ")

    # data for testing
    # from_city = "East London (ELS)"
    # to_city = "Abu Dhabi (BUS)"
    # date = "12-11-2022"

    try:
        logger.info(f'Start script. Waiting....')

        try:
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "onetrust-accept-btn-handler")))
            driver.find_element(By.ID, "onetrust-accept-btn-handler").click()
            time.sleep(2)
        except StaleElementReferenceException as e:
            logger.warning("Stale Element Reference Exception")
            pass
        except Exception as e:
            logger.warning(f"Cookie were passed. Exception: {e}")
            pass

        driver.find_element(By.XPATH, ".//a[@class='ts-session-expire--link']").click()
        data_input(from_city=from_city, to_city=to_city, date=date)
        scraped_data = scrape_data()
        save_to_csv("ffly_main.csv", scraped_data)
        print(scraped_data)

    except KeyboardInterrupt:
        logger.info("Keyboard Interrupt. Quit the driver!")
        driver.quit()
        logger.info(f'Module stopped working')
    except Exception as e:
        logger.exception(f'Stop script with errors:\n{e}')

    driver.quit()
    logger.info('Script successfully ended')


if __name__ == "__main__":
    main()

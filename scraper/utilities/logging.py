import logging
import logging.config
import sys


def get_logger(logger_name, DEBUG_FLAG, log_level):
    if DEBUG_FLAG == 0:
        logging.config.fileConfig('conf/logging.conf')
        logger = logging.getLogger(logger_name)
        logger.propagate = False
        level = logging.getLevelName(log_level)
        logger.setLevel(level)

    if DEBUG_FLAG == 1:
        logger = logging.getLogger('')
        level = logging.getLevelName(log_level)
        logger.setLevel(level)
        formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')
        fh = logging.FileHandler(f'logs/{logger_name}.log')
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)

        fh_f = logging.StreamHandler(sys.stdout)
        fh_f.setLevel(logging.DEBUG)
        fh_f.setFormatter(formatter)

        logger.addHandler(fh)
        logger.addHandler(fh_f)
    return logger
import os
from random import choice

import pandas as pd
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.firefox.service import Service as FirefoxService
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager


def get_driver(browser, use_arguments=True):
    user_agents = [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.62',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36',
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36',
    ]
    arguments_list = [
        '--headless',
        'window-size=1920,1080',
        f'user-agent={choice(user_agents)}',
    ]

    if use_arguments:
        arguments_list += [
            '--disable-logging',
            '--disable-extensions',
            '--disable-gpu',
            '--disable-infobars',
            '--no-sandbox',
            'enable-automation',
            '--disable-dev-shm-usage',
            '--incognito',
            '--disable-blink-features=AutomationControlled'
        ]

    if browser == 'Chrome':
        options = webdriver.ChromeOptions()
        [options.add_argument(argument) for argument in arguments_list]
        service = ChromeService(ChromeDriverManager().install())
        driver = webdriver.Chrome(service=service, options=options)
    elif browser == 'Firefox':
        options = webdriver.FirefoxOptions()
        [options.add_argument(argument) for argument in arguments_list]
        service = FirefoxService(GeckoDriverManager().install())
        driver = webdriver.Firefox(service=service, options=options)
    return driver


def save_to_csv(file_name, data_dict):
    df = pd.DataFrame(data_dict)
    if os.path.exists(file_name):
        df.to_csv(file_name, index=False, mode='a', header=False)
    else:
        outdir = file_name.split('/')[0]
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        df.to_csv(file_name, index=False)


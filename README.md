Create a simple web scraping script that searches for flights from city A to city B on the day D and returns first 2 to 3 flight results (only flight time, flight cost).

Parameters of the service:
A - user inputs the start city (you can provide 2 test start points as example, ex. East London (ELS) )
B - user inputs destination city (you can provide 2 test destination points, ex. Abu Dhabi (BUS) (ZVJ) )
D - user inputs day D (e.g. 22-10-2022)

Please provide the code with comments about the strategy of the solution. If the solution is not complete, provide the plan for future development iterations.
Test time - 4 hours

You can use any framework of the choice.

Start page for scraper:
https://fly2.emirates.com/CAB/IBE/SearchAvailability.aspx

Format of result from the script: [{‘flight time’: ‘time’,’flight cost’:’cost’},.....]
